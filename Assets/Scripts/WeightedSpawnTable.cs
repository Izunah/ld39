﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WeightedSpawnTable
{
    public GameObject go;
    [Range(0.0f, 1.0f)]
    public float spawnChance = 1.0f;
}