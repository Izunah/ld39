﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LD39/PlayerActions/Dock")]
public class Dock : PlayerAction
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if (!gameController.docked && !gameController.gameOver)
            gameController.playerController.AttemptDocking();
        else
        {
            if (gameController.docked)
                gameController.AddTextToList("<color=red>We are already docked.</color>");
            else
                gameController.AddTextToList("<color=red>We can't do this right now.</color>");
        }
    }
}