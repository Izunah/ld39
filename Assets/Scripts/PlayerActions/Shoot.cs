﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Shoot")]
public class Shoot : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if (!gameController.docked && !gameController.gameOver)
        {
            if (separatedText.Length > 1)
            {
                if (gameController.playerController.targetsInRange.ContainsKey(separatedText[1]))
                    gameController.playerController.currentTarget = gameController.playerController.targetsInRange[separatedText[1]];
                else
                {
                    gameController.AddTextToList("<color=red>Invalid target to shoot.</color>");
                    gameController.AddTextToList("<color=red>Spaces between targets name and</color>");
                    gameController.AddTextToList("<color=red>number are invalid. Try again.</color>");
                }
            }
            else
                gameController.AddTextToList("<color=red>No target specified.</color>");
        }
        else
            gameController.AddTextToList("<color=red>We can't do this right now.</color>");
    }
}