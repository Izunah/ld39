﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : ScriptableObject 
{
    public string weaponName;
    public string attackType;

    public float attackRange;
    public float attackDelay;

    public int powerDrain;

    public AudioClip weaponSfx;

    public abstract void DoAttack(Vector3 shotSpawn, GameObject target, string compareTag);
}