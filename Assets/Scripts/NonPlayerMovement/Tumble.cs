﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tumble : MonoBehaviour 
{
    public Vector3[] tumbleDirection = new Vector3[2];
    public float tumbleSpeed = 10.0f;

    private Vector3 tumble;

    private void Start()
    {
        tumble = new Vector3(Random.Range(tumbleDirection[0].x, tumbleDirection[1].x), 
            Random.Range(tumbleDirection[0].y, tumbleDirection[1].y), 
            Random.Range(tumbleDirection[0].z, tumbleDirection[1].z));
    }
    // Update is called once per frame
    void Update () 
	{
        transform.Rotate(tumble * tumbleSpeed * Time.deltaTime);
	}
}