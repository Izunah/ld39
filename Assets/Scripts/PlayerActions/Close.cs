﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Close")]
public class Close : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if (separatedText.Length > 1)
            gameController.CloseMenu(separatedText[1]);
        else
            gameController.CloseMenu(null);
    }
}