﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTextInput : MonoBehaviour 
{
    public InputField inputField;
    private GameController gameController;
    private bool actionFound = false;

    // Use this for initialization
    private void Awake()
    {
        gameController = GetComponent<GameController>();
        inputField.onEndEdit.AddListener(OnStringInput);
    }

    private void Start()
    {
        inputField.ActivateInputField();
    }

    private void OnStringInput(string text)
    {
        gameController.AddToPreviousPlayerInput(text);
        text = text.ToLower();

        char[] stringSeparaters = { ' ' };
        string[] separatedText = text.Split(stringSeparaters);

        for (int i = 0; i < gameController.playerActions.Length; i++)
        {
            PlayerAction action = gameController.playerActions[i];

            for (int names = 0; names < action.actionName.Length; names++)
            {
                if (action.actionName[names] == separatedText[0])
                {
                    actionFound = true;
                    action.DoAction(gameController, separatedText);
                }
            }
        }
        
        if (!actionFound)
        {
            gameController.AddTextToList("<color=red>Invalid action.</color> Type <color=orange>help</color>");
            gameController.AddTextToList("for a list of actions.");
        }
        else
            actionFound = false;

        ClearAndRefocusInputField();
    }

    private void ClearAndRefocusInputField()
    {
        gameController.UpdateText();
        inputField.ActivateInputField();
        inputField.text = null;
    }
}