﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Undock")]
public class Undock : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if(gameController.docked && !gameController.gameOver)
        {
            gameController.Undock();
        }
        else
            gameController.AddTextToList("<color=red>We are not currently docked.</color>");
    }
}