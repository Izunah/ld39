﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Float : MonoBehaviour 
{
    public float floatSpeed = 10.0f;
    private Vector3 moveDirection;

    private void Start()
    {
        Vector3 point = transform.position + Random.insideUnitSphere * 5.0f;
        point.y = 0.0f;

        moveDirection = (point - transform.position).normalized;
    }

    void Update()
    {
        transform.Translate(moveDirection * floatSpeed * Time.deltaTime);
    }
}