﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/Weapons/RangedWeapon")]
public class RangedWeapon : Weapon 
{
    public GameObject projectile;

    public override void DoAttack(Vector3 shotSpawn, GameObject target, string compareTag)
    {
        GameObject go = (GameObject)Instantiate(projectile, shotSpawn, Quaternion.identity);
        go.GetComponent<Bullet>().targetToHit = compareTag;

        Vector3 travelDirection = (target.transform.position - shotSpawn);
        go.transform.rotation = Quaternion.Euler(travelDirection);
        go.transform.LookAt(target.transform.position);

    }
}