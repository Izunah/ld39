﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOverTime : MonoBehaviour 
{
    public float time = 2.0f;

    private void Start()
    {
        Destroy(gameObject, time);
    }
}