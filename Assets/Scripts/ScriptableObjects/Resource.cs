﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/Drops/Resource")]
public class Resource : ScriptableObject 
{
    public string resourceName;
    public GameObject itemDrop;
    private PlayerController playerController;

    public void DropResource(Vector3 location)
    {
        GameObject drop = (GameObject)Instantiate(itemDrop, location, Quaternion.identity);
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        if (playerController != null)
            playerController.AddToAvailableDrops(drop);
        else
            Debug.LogError("Can't find player!");
    }
}