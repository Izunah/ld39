﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Pickup")]
public class Pickup : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if(!gameController.docked && !gameController.gameOver)
        {
            if (separatedText.Length > 1)
                gameController.playerController.AttemptPickup(separatedText[1]);
            else
                gameController.AddTextToList("<color=red>No object specified.</color>");
        }
        else
            gameController.AddTextToList("<color=red>We can't do this right now.</color>");
    }
}