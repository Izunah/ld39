﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Upgrade")]
public class Upgrade : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if(gameController.docked && !gameController.gameOver)
        {
            if (separatedText.Length > 1)
            {
                if(separatedText[1] == "station")
                    gameController.stationController.HandleUpgrade();
                else
                    gameController.HandleUpgrade(separatedText[1]);
            }
            else
                gameController.AddTextToList("<color=red>Upgrade type not specified.</color>");
        }
        else
        {
            if (!gameController.docked)
                gameController.AddTextToList("<color=red>We need to be docked to do this.</color>");
            else
                gameController.AddTextToList("<color=red>We can't do this right now.</color>");
        }
    }
}