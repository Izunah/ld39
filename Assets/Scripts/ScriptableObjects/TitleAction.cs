﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TitleAction : ScriptableObject 
{
    public string[] actionName;

    public abstract void DoAction(TitleController titleController);
}