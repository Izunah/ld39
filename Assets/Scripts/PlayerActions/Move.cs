﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Move")]
public class Move : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if (!gameController.docked && !gameController.gameOver)
        {
            if (separatedText.Length > 1)
            {
                string requestedHeading = separatedText[1];
                float desiredHeading = 9000.0f;

                if (requestedHeading == "up" || requestedHeading == "north")
                    desiredHeading = 0.0f;
                else if (requestedHeading == "down" || requestedHeading == "south")
                    desiredHeading = 180.0f;
                else if (requestedHeading == "left" || requestedHeading == "west")
                    desiredHeading = 270.0f;
                else if (requestedHeading == "right" || requestedHeading == "east")
                    desiredHeading = 90.0f;
                else
                    gameController.AddTextToList("<color=red>Invalid direction specificied.</color>");

                if (desiredHeading != 9000.0f)
                    gameController.playerController.UpdateHeading(desiredHeading);
            }
            else
            {
                gameController.AddTextToList("<color=red>No direction specified.</color>");
            }
        }
        else
            gameController.AddTextToList("<color=red>We can't do this right now.</color>");
    }
}