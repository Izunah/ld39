﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleController : MonoBehaviour 
{
    public GameObject helpOverlay;
    public TitleAction[] titleActions;

    private void Start()
    {
        helpOverlay.SetActive(false);
    }

    public void OpenHelp()
    {
        helpOverlay.SetActive(true);
    }

    public void CloseHelp()
    {
        helpOverlay.SetActive(false);
    }

    public void HandleExit()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
}