﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour 
{
    public GameObject followTarget;
    public Vector3 offset;

    private void Start()
    {
        followTarget = GameObject.FindGameObjectWithTag("Player");   
    }

    private void FindTarget()
    {
        followTarget = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void LateUpdate () 
	{
        if (followTarget != null)
            transform.position = followTarget.transform.position + offset;
        else
            FindTarget();
	}
}