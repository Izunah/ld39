﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/SpawnTables/Basic")]
public class SpawnTable : ScriptableObject
{
    public WeightedSpawnTable[] spawnTable;
}