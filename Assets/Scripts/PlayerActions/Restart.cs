﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName ="LD39/PlayerActions/Restart")]
public class Restart : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if (gameController.gameOver)
        {
            gameController.HandleRestart();
        }
        else
            gameController.AddTextToList("<color=red>That is not necessary right now.</color>");
    }
}