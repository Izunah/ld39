﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Scan")]
public class Scan : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if(!gameController.docked && !gameController.gameOver)
        {
            if (separatedText.Length > 1)
                gameController.playerController.Scan(separatedText[1]);
            else
                gameController.AddTextToList("<color=red>No target specified.</color>");
        }
        else
            gameController.AddTextToList("<color=red>We can't do this right now.</color>");
    }
}