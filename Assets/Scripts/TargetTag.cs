﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetTag : MonoBehaviour 
{
    public string targetTag = "";
    public string assignedId = null;
    public Text displayId;

    public void updateDisplayId(string id)
    {
        assignedId = id;
        displayId.text = assignedId;
    }
}