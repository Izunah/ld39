﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public static class PlayerValues
{
    //Hull
    private static int repairHullPrice = 1; //Cost to repair hull per missing point
    private static int upgradeHullCost = 5;
    public static int hullLevel = 1;
    public static int maxHull = 20;
    public static int currentHull = 20;

    public static int hullIncrease = 5;
    private static float hullCostMultiplier = 1.25f;

    //Power
    private static int upgradePowerCost = 5;
    public static int powerLevel = 1;
    public static int currentPower = 100;
    public static int maxPower = 100;

    public static int powerIncrease = 10;
    private static float powerCostMultiplier = 1.25f;

    public static int scanPowerCost = 2;

    //Resource
    public static string repairResource = "scrap";

    public static int RepairHullCost()
    {
        int cost = (maxHull - currentHull) * repairHullPrice;
        return cost;
    }

    public static int IncreaseShipPowerCost()
    {
        int cost = Mathf.RoundToInt(Mathf.Max(upgradePowerCost, Mathf.Pow(((powerLevel - 1) *upgradePowerCost), powerCostMultiplier)));
        return cost;
    }

    public static int IncreaseShipHullCost()
    {
        int cost = Mathf.RoundToInt(Mathf.Max(upgradeHullCost, Mathf.Pow(((hullLevel - 1) * upgradeHullCost), hullCostMultiplier)));
        return cost;
    }
}

public class PlayerController : MonoBehaviour 
{
    [HideInInspector] public GameObject currentTarget = null;
    [HideInInspector] public GameObject pickupTarget = null;
    [HideInInspector] public GameController gameController;

    public GameObject[] ranges;
    public GameObject shotSpawn;
    public AudioSource playerAudio;
    public AudioClip pickupItemSound;

    public Weapon currentWeapon;

    //Movement Speed Related
    public float currentSpeed = 3.0f;
    public int throttle = 20;
    private float minSpeed = 3.0f;
    private float maxSpeed = 15.0f;
    private float drainSpeed = 2.5f;

    private float scanRadius = 20.0f;
    private float pickupRadius = 10.0f;
    private float scanRangeFadeTime = 2.0f;

    //Bools
    private bool attackReady = true;
    private bool attemptingPickup = false;
    private bool deathByPower = false;

    //Timers
    private float attackAvailableIn = 0.0f;
    private float fadeScanRangeIn = 0.0f;
    private float drainPowerTimer = 0.0f;
    private float deathTimer = 0.0f;
    private float deathDelay = 1.0f;

    //Movement variables
    private float desiredHeading = 0.0f;
    private float rotLerpPercent = 0.0f;
    private float rotationSpeed = 0.075f; //How fast we rotate

    //Dictionaries
    public Dictionary<string, GameObject> targetsInRange = new Dictionary<string, GameObject>();
    public Dictionary<string, GameObject> dropsAvailable = new Dictionary<string, GameObject>();


    private void Awake()
    {
        playerAudio = GetComponent<AudioSource>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        if (gameController == null)
            Debug.LogError("Player couldn't find the game controller.");

        drainPowerTimer = Time.time + drainSpeed;
    }

    private void Start()
    {
        UpdateRanges();
        for (int i = 0; i < ranges.Length; i++)
        {
            ToggleRange(ranges[i], false);
        }

        GetComponent<SphereCollider>().radius = scanRadius;
        PlayerValues.currentHull = PlayerValues.maxHull;
        PlayerValues.currentPower = PlayerValues.maxPower;
    }

    private void Update()
    {
        //Timers
        CheckAttackTimer();
        CheckScanRangeFadeTimer();
        CheckDrainTimer();

        if(deathByPower)
        {
            if(deathTimer < Time.time)
            {
                deathTimer = Mathf.Infinity;
                gameController.GameOver("Ship ran out of power.");
            }
        }

        if (transform.rotation.y != desiredHeading)
        {
            rotLerpPercent += rotationSpeed * Time.deltaTime;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, desiredHeading, 0), rotLerpPercent);
        }

        transform.Translate(Vector3.forward * currentSpeed * Time.deltaTime);
        WrapAroundMap();

        if (currentTarget != null)
        {
            ToggleRange(ranges[1], true);
            if (attackReady)
                Attack();
        }
        else
            ToggleRange(ranges[1], false);

        if (attemptingPickup)
        {
            ToggleRange(ranges[2], true);

            if (Vector3.Distance(transform.position, pickupTarget.transform.position) < pickupRadius)
            {
                HandlePickup();
            }
        }
        else
            ToggleRange(ranges[2], false);
    }

    private void WrapAroundMap()
    {
        if(transform.position.x > gameController.worldSize / 2)
        {
            transform.position = new Vector3(transform.position.x - gameController.worldSize, 0.0f, transform.position.z);
        }
        
        if(transform.position.x < -gameController.worldSize / 2)
        {
            transform.position = new Vector3(transform.position.x + gameController.worldSize, 0.0f, transform.position.z);
        }

        if(transform.position.z > gameController.worldSize / 2)
        {
            transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z - gameController.worldSize);
        }

        if(transform.position.z < -gameController.worldSize / 2)
        {
            transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z + gameController.worldSize);
        }
    }

    private void ToggleRange(GameObject range, bool active)
    {
        range.SetActive(active);
    }

    private void UpdateRanges()
    {
        ranges[0].GetComponent<RectTransform>().sizeDelta = new Vector2(scanRadius * 2.15f, scanRadius * 2.15f);
        ranges[1].GetComponent<RectTransform>().sizeDelta = new Vector2(currentWeapon.attackRange * 2, currentWeapon.attackRange * 2);
        ranges[2].GetComponent<RectTransform>().sizeDelta = new Vector2(pickupRadius * 2, pickupRadius * 2);
    }

    private void CheckAttackTimer()
    {
        if (Time.time > attackAvailableIn)
        {
            attackAvailableIn = Mathf.Infinity;
            attackReady = true;
        }
    }

    private void CheckScanRangeFadeTimer()
    {
        if(Time.time > fadeScanRangeIn)
        {
            fadeScanRangeIn = Mathf.Infinity;
            ToggleRange(ranges[0], false);
        }
    }

    private void CheckDrainTimer()
    {
        if(drainPowerTimer < Time.time)
        {
            if(!deathByPower)
            {
                AdjustPower(-1);
                drainPowerTimer = Time.time + drainSpeed;
            }
        }
    }

    private string GenerateTargetID(GameObject other, Dictionary<string, GameObject> dictionary)
    {
        string id = null;
        int number = 0;
        string theTag = other.GetComponent<TargetTag>().targetTag;

        id = theTag + number.ToString();

        while (dictionary.ContainsKey(id))
        {
            number++;
            id = theTag + number.ToString();
        }

        other.GetComponent<TargetTag>().updateDisplayId(id);
        return id;
    }

    private void AddToTargetDictionary(string key, GameObject value)
    {
        targetsInRange.Add(key, value);
    }

    private void RemoveFromTargetDictionary(GameObject go)
    {
        if (currentTarget == go)
            currentTarget = null;

        if (pickupTarget == go)
        {
            pickupTarget = null;
            attemptingPickup = false;
        }

        targetsInRange.Remove(go.GetComponent<TargetTag>().assignedId);
        go.GetComponent<TargetTag>().updateDisplayId(null);
    }

    private void UpdateTarget(GameObject go, bool overwrite = false)
    {
        if (!overwrite)
        {
            if(go.GetComponent<TargetTag>().targetTag == "enemy" && currentTarget == null)
            {
                currentTarget = go;
            }
        }
        else
            currentTarget = go;
    }

    private void Attack()
    {
        float distanceFromTarget = Vector3.Distance(transform.position, currentTarget.transform.position);

        if(distanceFromTarget < currentWeapon.attackRange)
        {
            currentWeapon.DoAttack(shotSpawn.transform.position, currentTarget, "Target");
            AdjustPower(currentWeapon.powerDrain);
            playerAudio.clip = currentWeapon.weaponSfx;
            playerAudio.Play();

            attackReady = false;
            attackAvailableIn = Time.time + currentWeapon.attackDelay;
        }
    }
    
    private void HandlePickup()
    {
        playerAudio.clip = pickupItemSound;
        playerAudio.Play();

        gameController.playerInventory.AddToInventory(pickupTarget.GetComponent<TargetTag>().targetTag, 1);
        dropsAvailable.Remove(pickupTarget.GetComponent<TargetTag>().assignedId);

        Destroy(pickupTarget);
        pickupTarget = null;
        attemptingPickup = false;
    }

    private void AdjustPowerDrain()
    {
        switch (throttle)
        {
            case 20:
                drainSpeed = 2.5f;
                break;
            case 40:
                drainSpeed = 1.75f;
                break;
            case 60:
                drainSpeed = 1.325f;
                break;
            case 80:
                drainSpeed = 0.75f;
                break;
            case 100:
                drainSpeed = 0.5f;
                break;
            default:
                drainSpeed += 0;
                break;
        }

        drainPowerTimer = Time.time + drainSpeed;
        AdjustPower(-1);
    }
    
    private void AdjustPower(int amount)
    {
        PlayerValues.currentPower += amount;

        if(PlayerValues.currentPower <= 0)
        {
            PlayerValues.currentPower = 0;
            StartGameOver();
        }

        if(PlayerValues.currentPower > PlayerValues.maxPower)
        {
            PlayerValues.currentPower = PlayerValues.maxPower;
        }
    }

    private void AdjustHull(int amount)
    {
        PlayerValues.currentHull += amount;

        if (PlayerValues.currentHull <= 0)
        {
            PlayerValues.currentHull = 0;
            gameController.GameOver("Ship was destroyed.");
        }

        if (PlayerValues.currentHull > PlayerValues.maxHull)
        {
            PlayerValues.currentHull = PlayerValues.maxHull;
        }
    }

    private void StartGameOver()
    {
        deathByPower = true;
        deathTimer = Time.time + deathDelay;
        GetComponentInChildren<ParticleSystem>().Stop();
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Target"))
        {
            string targetId = GenerateTargetID(other.gameObject, targetsInRange);
            AddToTargetDictionary(targetId, other.gameObject);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Target"))
        {
            RemoveFromTargetDictionary(other.gameObject);
        }
    }

    public void UpdateHeading(float newHeading)
    {
        desiredHeading = newHeading;
        rotLerpPercent = 0.0f;
    }

    public void AddToAvailableDrops(GameObject drop)
    {
        string targetId = GenerateTargetID(drop, dropsAvailable);
        dropsAvailable.Add(targetId, drop);
    }

    public void Scan(string target)
    {
        ToggleRange(ranges[0], true);

        if(targetsInRange.ContainsKey(target))
        {
            GameObject ourTarget = targetsInRange[target];
            string targetType = ourTarget.GetComponent<TargetTag>().targetTag;
            AdjustPower(PlayerValues.scanPowerCost);
            switch(targetType)
            {
                case "asteroid":
                    Asteroid asteroidScript = ourTarget.GetComponent<Asteroid>();
                    gameController.AddTextToList("<color=green>Found " + asteroidScript.yield + " amounts of " + asteroidScript.resource.resourceName + ".</color>");
                    break;
                default:
                    gameController.AddTextToList("<color=red>Scanning this object returns no information.</color>");
                    break;
            }
        }           
        else
        {
            gameController.AddTextToList("<color=red>Target not found in scan range.</color>");
            gameController.AddTextToList("<color=red>Spaces between targets name and</color>");
            gameController.AddTextToList("<color=red>number are invalid. Try again.</color>");
        }

        fadeScanRangeIn = Time.time + scanRangeFadeTime;
    }

    public void AttemptPickup(string drop)
    {
        if(dropsAvailable.ContainsKey(drop))
        {
            pickupTarget = dropsAvailable[drop];
            attemptingPickup = true;
        }
        else
        {
            gameController.AddTextToList("<color=red>Target not found nearby.</color>");
            gameController.AddTextToList("<color=red>Spaces between targets name and</color>");
            gameController.AddTextToList("<color=red>number are invalid. Try again.</color>");
        }
    }

    public void AdjustThrottle(string adjust)
    {
        if (adjust == "max")
        {
            throttle = 100;
            currentSpeed = maxSpeed;
        }
        else if (adjust == "min")
        {
            throttle = 20;
            currentSpeed = minSpeed;
        }
        else if (adjust == "up")
        {
            if (throttle != 100)
            {
                throttle += 20;
                currentSpeed += maxSpeed / minSpeed;
            }
        }
        else if (adjust == "down")
        {
            if (throttle != 20)
            {
                throttle -= 20;
                currentSpeed -= maxSpeed / minSpeed;
            }
        }
        else
            gameController.AddTextToList("<color=red>Invalid throttle adjustment.</color>");

        AdjustPowerDrain();
        gameController.AdjustThrottle("Throttle: " + throttle.ToString() + "%");
    }

    public void AttemptDocking()
    {
        Vector3 dockPoint = gameController.playerSpawn.position;
        float distToDock = Vector3.Distance(transform.position, dockPoint);

        if(distToDock < pickupRadius * 2.5)
        {
            transform.position = dockPoint;
            PlayerValues.currentPower = PlayerValues.maxPower;
            Destroy(gameObject);
            gameController.docked = true;
            gameController.ShowMenu("station");
        }
        else
        {
            gameController.AddTextToList("<color=red>Too far from the station to dock.</color>");
        }
    }
}