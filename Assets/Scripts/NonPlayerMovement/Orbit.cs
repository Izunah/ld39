﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour 
{
    public GameObject orbitThis;
    public float orbitSpeed;

	// Update is called once per frame
	void Update () 
	{
        transform.RotateAround(orbitThis.transform.position, orbitThis.transform.up, orbitSpeed * Time.deltaTime);
	}
}