﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour 
{

    public GameObject enemySpawnSpot;
    public GameObject enemyPrefab;

    public float spawnRadius = 5.0f;

    public int enemiesToSpawn = 2;


    private void Start()
    {
        SpawnEnemies();
    }

    private void SpawnEnemies()
    {
        for (int i = 0; i < enemiesToSpawn; i++)
        {
            Vector3 spawnPos = enemySpawnSpot.transform.position + Random.insideUnitSphere * spawnRadius;
            spawnPos.y = 0.0f;

            GameObject go = (GameObject)Instantiate(enemyPrefab, spawnPos, Quaternion.identity);
        }
    }
}