﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/TitleActions/TitleClose")]
public class Title_Close : TitleAction 
{
    public override void DoAction(TitleController titleController)
    {
        titleController.CloseHelp();
    }
}