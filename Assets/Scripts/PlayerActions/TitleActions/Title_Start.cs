﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/TitleActions/TitleStart")]
public class Title_Start : TitleAction 
{
    public override void DoAction(TitleController titleController)
    {
        titleController.StartGame();
    }
}