﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationController : MonoBehaviour 
{
    [HideInInspector] public GameController gameController;

    public int refillCost = 2;
    public int stationUpgradeCost = 10;
    public float stationUpgradeMultiplier = 1.25f;

    public int maxPower = 3000;
    public int maxPowerIncrease = 600;

    public int currentPower = 1800;
    private int powerDrain = 5; //Drain amount per second
    private float drainSpeed = 1.0f; //Drain delay (one second)
    private float drainTimer = 0.0f; 

    private int stationLevel = 1;
    private string refillResource = "scrap";
    private string powerDrainGameOver = "The station ran out of power. All hope is now lost.";

    private void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    private void Start()
    {
        currentPower = maxPower;
    }

    private void Update()
    {
        if(!gameController.docked && !gameController.gameOver)
        {
            if(drainTimer < Time.time)
            {
                DrainPower();
                drainTimer = Time.time + drainSpeed;
            }
        }
    }

    private void DrainPower()
    {
        currentPower -= powerDrain;

        if(currentPower <= 0)
        {
            currentPower = 0;
            gameController.GameOver(powerDrainGameOver);
        }
    }

    public int RefillMaxPowerCost()
    {
        if (currentPower < maxPower)
        {
            int cost = Mathf.Max((refillCost * (Mathf.RoundToInt((maxPower - currentPower) / maxPowerIncrease))), refillCost);
            return cost;
        }
        else
            return 0;
    }

    public int UpgradeStationCost()
    {
        int cost = Mathf.RoundToInt(Mathf.Max(stationUpgradeCost, Mathf.Pow(((stationLevel - 1) * stationUpgradeCost), stationUpgradeMultiplier)));
        return cost;
    }

    public void ResetDrainTimer()
    {
        drainTimer = Time.time + drainSpeed;
    }

    public void HandleRefill(string refill)
    {
        if (currentPower != maxPower)
        {
            if (refill == "station")
            {
                if (gameController.playerInventory.SubtractFromInventory(refillResource, RefillMaxPowerCost()))
                {
                    currentPower = maxPower;
                }
                else
                    gameController.AddTextToList("<color=red>We cannot afford that.</color>");
            }
            else if (refill == "partial")
            {
                if (gameController.playerInventory.SubtractFromInventory(refillResource, refillCost))
                {
                    currentPower += maxPowerIncrease;
                }
                else
                    gameController.AddTextToList("<color=red>We cannot afford that.</color>");
            }
            else
                gameController.AddTextToList("<color=red>Invalid refill type specified.</color>");
        }
        else
            gameController.AddTextToList("<color=red>The station is full already.</color>");

        gameController.UpdateStationMenu();
    }

    public void HandleUpgrade()
    {
        if (gameController.playerInventory.SubtractFromInventory(refillResource, UpgradeStationCost()))
        {
            maxPower += maxPowerIncrease;
            currentPower = maxPower;
            stationLevel++;
        }
        else
            gameController.AddTextToList("<color=red>We cannot afford that.</color>");

        gameController.UpdateStationMenu();
    }
}