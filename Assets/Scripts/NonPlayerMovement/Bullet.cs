﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour 
{
    public float speed = 10.0f;
    public int damage = 1;
    public string targetToHit;

    private void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag(targetToHit))
        {
            other.SendMessage("TakeDamage", damage);
            Destroy(gameObject);
        }
    }
}