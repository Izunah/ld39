﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleTextInput : MonoBehaviour 
{
    public InputField inputField;
    private TitleController titleController;

    // Use this for initialization
    private void Awake()
    {
        titleController = GetComponent<TitleController>();
        inputField.onEndEdit.AddListener(OnStringInput);
    }

    private void Start()
    {
        inputField.ActivateInputField();
    }

    private void OnStringInput(string text)
    {
        text = text.ToLower();

        char[] stringSeparaters = { ' ' };
        string[] separatedText = text.Split(stringSeparaters);

        for (int i = 0; i < titleController.titleActions.Length; i++)
        {
            TitleAction action = titleController.titleActions[i];

            for (int names = 0; names < action.actionName.Length; names++)
            {
                if (action.actionName[names] == separatedText[0])
                {
                    action.DoAction(titleController);
                }
            }
        }

        ClearAndRefocusInputField();
    }

    private void ClearAndRefocusInputField()
    {
        inputField.ActivateInputField();
        inputField.text = null;
    }
}