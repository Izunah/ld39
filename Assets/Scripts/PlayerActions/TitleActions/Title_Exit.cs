﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/TitleActions/TitleExit")]
public class Title_Exit : TitleAction 
{
    public override void DoAction(TitleController titleController)
    {
        titleController.HandleExit();
    }
}