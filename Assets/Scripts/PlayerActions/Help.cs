﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Help")]
public class Help : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        gameController.Help();
    }
}