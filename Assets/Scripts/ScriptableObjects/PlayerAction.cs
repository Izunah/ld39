﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerAction : ScriptableObject 
{
    public string[] actionName;

    public abstract void DoAction(GameController gameController, string[] separatedText);
}