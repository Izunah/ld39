﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour 
{
    public Text inventoryDisplay;
    public Dictionary<string, int> inventory = new Dictionary<string, int>();

    private void Start()
    {
        DisplayInventory();
    }

    private int CheckMineralAmount(string resource)
    {
        if (inventory.ContainsKey(resource))
            return inventory[resource];
        else
            return 0;
    }

    public void AddToInventory(string item, int amount)
    {
        if (inventory.ContainsKey(item))
            inventory[item] += amount;
        else
            inventory.Add(item, amount);

        Debug.Log(item + ": " + inventory[item]);
        DisplayInventory();
    }

    public void DisplayInventory()
    {
        inventoryDisplay.text = "";

        foreach(KeyValuePair<string, int> keyandvalues in inventory)
            inventoryDisplay.text += keyandvalues.Key + ": " + keyandvalues.Value + "\n";
    }

    public bool SubtractFromInventory(string resource, int amount)
    {
        if (CheckMineralAmount(resource) >= amount)
        {
            inventory[resource] -= amount;
            DisplayInventory();
            return true;
        }
        else
            return false;
    }
}