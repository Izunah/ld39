﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour 
{
    public float health = 10;

    public Resource resource;
    public string yield = "";
    public int minYield = 0;
    public int maxYield = 1;

    public int dropRange = 2;
    public AudioClip deathSound;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GameObject.FindGameObjectWithTag("GameController").GetComponent<AudioSource>();
    }

    private void Death()
    {
        int dropAmount = Random.Range(minYield, maxYield + 1);

        for (int i = 0; i < dropAmount; i++)
        {
            Vector3 dropSpawn = transform.position + Random.insideUnitSphere * dropRange;
            dropSpawn.y = 0.0f;
            resource.DropResource(dropSpawn);
        }

        transform.parent.GetComponent<AsteroidBelt>().currentAsteroids--;
        audioSource.clip = deathSound;
        audioSource.Play();
        Destroy(gameObject);
    }

    public void TakeDamage(float damage)
    {
        health -= damage;

        if (health <= 0)
        {
            health = 0;
            Death();
        }
    }
}