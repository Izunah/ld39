﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/TitleActions/TitleHelp")]
public class Title_Help : TitleAction 
{
    public override void DoAction(TitleController titleController)
    {
        titleController.OpenHelp();
    }
}