﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceUp : MonoBehaviour 
{
	// Update is called once per frame
	void LateUpdate () 
	{
        transform.rotation = Quaternion.Euler(90, -transform.parent.rotation.y, 0);
	}
}