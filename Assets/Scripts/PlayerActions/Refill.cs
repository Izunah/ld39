﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Refill")]
public class Refill : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if(gameController.docked && !gameController.gameOver)
        {
            if (separatedText.Length > 1)
            {
                if(separatedText[1] != "ship")
                    gameController.stationController.HandleRefill(separatedText[1]);
                else
                    gameController.HandleRepair();
            }
            else
                gameController.AddTextToList("<color=red>Refill type not specified.</color>");
        }
        else
        {
            if (!gameController.docked)
                gameController.AddTextToList("<color=red>We need to be docked to do this.</color>");
            else
                gameController.AddTextToList("<color=red>We can't do this right now.</color>");
        }
    }
}