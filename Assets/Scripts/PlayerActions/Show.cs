﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Show")]
public class Show : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if (!gameController.docked && !gameController.gameOver)
        {
            if (separatedText.Length > 1)
                gameController.ShowMenu(separatedText[1]);
            else
                gameController.AddTextToList("<color=red>No option specified.</color>");
        }
        else
            gameController.AddTextToList("<color=red>We can't do this right now.</color>");
    }
}