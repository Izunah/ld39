﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBelt : MonoBehaviour 
{
    public SpawnTable asteroidsToSpawn;
    public GameObject minimapIcon;

    /*[HideInInspector]*/ public int currentAsteroids = 0;
    public int startingAsteroids;
    public int maximumAsteroids;

    public float asteroidBeltSize;
    public float spawnAsteroidDelay;

    private GameObject asteroid;
    private float spawnNextAsteroid = 0.0f;
    public bool onMinimap = false;

    private void Start()
    {
        minimapIcon.SetActive(false);
        for (int i = 0; i < startingAsteroids; i++)
        {
            SpawnAsteroids();
        }

        spawnNextAsteroid = Time.time + spawnAsteroidDelay;
    }

    private void Update()
    {
        if(currentAsteroids < maximumAsteroids)
        {
            if(spawnNextAsteroid < Time.time)
            {
                SpawnAsteroids();
            }
        }
        else
            spawnNextAsteroid = Time.time + spawnAsteroidDelay + Random.Range(-spawnAsteroidDelay / 2, spawnAsteroidDelay * 1.5f);
    }

    public void SpawnAsteroids()
    {
        float weight = Random.Range(0.0f, 1.0f);
        asteroid = null;

        for (int i = 0; i < asteroidsToSpawn.spawnTable.Length; i++)
        {
            if (weight <= asteroidsToSpawn.spawnTable[i].spawnChance)
            {
                asteroid = asteroidsToSpawn.spawnTable[i].go;
                break;
            }
        }

        if(asteroid != null)
        {
            Vector3 spawnPos = transform.position + Random.insideUnitSphere * asteroidBeltSize;
            spawnPos.y = 0.0f;

            Instantiate(asteroid, spawnPos, Quaternion.identity, transform);
            spawnNextAsteroid = Time.time + spawnAsteroidDelay;
            currentAsteroids++;
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, asteroidBeltSize);
    }

    public void OnTriggerEnter(Collider other)
    {
        if(!onMinimap)
        {
            if(other.gameObject.CompareTag("Player"))
            {
                minimapIcon.SetActive(true);
                onMinimap = true;
            }
        }
    }
}