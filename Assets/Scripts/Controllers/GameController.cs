﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour 
{
    [HideInInspector]public PlayerController playerController;
    [HideInInspector]public StationController stationController;
    [HideInInspector]public PlayerInventory playerInventory;

    public GameObject mapOverlay;
    public GameObject stationOverlay;
    public GameObject throttleDisplay;
    public GameObject gameOverOverlay;
    public GameObject menuOverlay;

    public Transform playerSpawn;

    public Text[] stationChoices;
    public Text trackedStats;
    public Text gameOverText;
    public InputField inputField;

    public bool docked = true;
    public bool gameOver = false;
    public bool menuOpen = false;

    //World generation
    public float worldSize = 500;

    public SpawnTable asteroidBeltTable;
    public List<GameObject> beltSpawns = new List<GameObject>();
    public List<GameObject> asteroidBelts = new List<GameObject>();
    public int numberOfBelts = 12;

    public PlayerAction[] playerActions;
    public Text previousTextDisplay;
    public GameObject playerPrefab;

    private int maxInputListSize = 14;
    private List<string> previousPlayerInput = new List<string>();
    private List<string> pastPlayerPhrases = new List<string>();

    private void Awake()
    {
        stationController = GameObject.FindGameObjectWithTag("Station").GetComponent<StationController>();
        playerInventory = GetComponent<PlayerInventory>();
    }

    private void Start()
    {
        mapOverlay.SetActive(false);
        stationOverlay.SetActive(false);
        gameOverOverlay.SetActive(false);
        menuOverlay.SetActive(false);
        GenerateSpace();

        previousTextDisplay.text = "";
        AddTextToList("Collect scrap from asteroids to");
        AddTextToList("power the space station. If power");
        AddTextToList("depletes the game ends.");
        AddTextToList("Good luck.");
        AddTextToList("Type <color=orange>undock</color> to begin!");
        UpdateText();
    }

    private void Update()
    {
        UpdateStats();

        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            if(pastPlayerPhrases.Count > 0)
            {
                int lastMessage = pastPlayerPhrases.Count - 1;
                inputField.text = pastPlayerPhrases[lastMessage];
            }
        }
    }

    private void UpdateStats()
    {
        trackedStats.text = string.Format("Station Power: {0}/{1} \nShip Power: {2}/{3} \nShip Hull: {4}/{5}", stationController.currentPower, stationController.maxPower,
            PlayerValues.currentPower, PlayerValues.maxPower, PlayerValues.currentHull, PlayerValues.maxHull);
    }

    private void GenerateSpace()
    {
        for (int i = 0; i < numberOfBelts; i++)
        {
            int spawn = Random.Range(0, beltSpawns.Count);
            Vector3 spawnPos = beltSpawns[spawn].transform.position;
            beltSpawns.RemoveAt(spawn);

            GameObject asteroidPrefab = PickAsteroidBelt();
            GameObject generatedBelt = (GameObject)Instantiate(asteroidPrefab, spawnPos, Quaternion.identity, transform);
            
            asteroidBelts.Add(generatedBelt);
        }
        beltSpawns[0].transform.parent.gameObject.SetActive(false);
        beltSpawns.Clear();
    }

    private GameObject PickAsteroidBelt()
    {
        float weight = Random.Range(0.0f, 1.0f);
        GameObject asteroid = null;

        for (int i = 0; i < asteroidBeltTable.spawnTable.Length; i++)
        {
            if (weight <= asteroidBeltTable.spawnTable[i].spawnChance)
            {
                asteroid = asteroidBeltTable.spawnTable[i].go;
                break;
            }
        }

        return asteroid;
    }

    private void SpawnPlayer(Vector3 spawnPos)
    {
        spawnPos.y = 0.0f;
        GameObject player = (GameObject)Instantiate(playerPrefab, spawnPos, Quaternion.identity);
        playerController = player.GetComponent<PlayerController>();
        throttleDisplay.SetActive(true);
    }

    private void IsListFull()
    {
        if(previousPlayerInput.Count > maxInputListSize)
        {
            previousPlayerInput.RemoveAt(0);
        }
    }

    private void ClearPreviousTextDisplay()
    {
        previousTextDisplay.text = null;
    }

    public void UpdateStationMenu()
    {
        stationOverlay.SetActive(true);
        throttleDisplay.SetActive(false);

        stationChoices[0].text = string.Format("<color=orange>Refill Station</color> - Costs {0} scrap " +
            "| Restores <color=green>All</color> Station Power", stationController.RefillMaxPowerCost());

        stationChoices[1].text = string.Format("<color=orange>Refill Partial</color> - Costs {0} scrap " +
            "| Restores <color=green>{1}</color> Power", stationController.refillCost, stationController.maxPowerIncrease);

        stationChoices[2].text = string.Format("<color=orange>Repair Ship</color> - Costs {0} scrap " +
            "| Restores <color=green>All</color> Ship Hull", PlayerValues.RepairHullCost());

        stationChoices[3].text = string.Format("<color=orange>Upgrade Station</color> - Costs {0} scrap " +
            "| Increases Max Station Power By: <color=green>{1}</color>", stationController.UpgradeStationCost(), stationController.maxPowerIncrease);

        stationChoices[4].text = string.Format("<color=orange>Upgrade Power</color> - Costs {0} scrap " +
            "| Increases Max Ship Power By: <color=green>{1}</color>", PlayerValues.IncreaseShipPowerCost(), PlayerValues.powerIncrease);

        stationChoices[5].text = string.Format("<color=orange>Upgrade Hull</color> - Costs {0} scrap " +
            "| Increases Ship Hull By: <color=green>{1}</color>", PlayerValues.IncreaseShipHullCost(), PlayerValues.hullIncrease);
    }

    public void AddToPreviousPlayerInput(string text)
    {

        pastPlayerPhrases.Add(text);
        AddTextToList(text);
    }

    public void AddTextToList(string text)
    {
        previousPlayerInput.Add(text + "\n");
        IsListFull();
    }

    public void UpdateText()
    {
        ClearPreviousTextDisplay();

        for (int i = 0; i < previousPlayerInput.Count; i++)
        {
            previousTextDisplay.text += previousPlayerInput[i];
        }
    }
    
    public void Undock()
    {
        stationController.ResetDrainTimer();
        docked = false;
        stationOverlay.SetActive(false);
        SpawnPlayer(playerSpawn.position);
    }

    public void ShowMenu(string menu)
    {
        if(menu == "map")
            mapOverlay.SetActive(true);

        if(menu == "menu")
        {
            menuOverlay.SetActive(true);
            menuOpen = true;
            Time.timeScale = 0.0001f;
        }

        if (menu == "station")
        {
            if (docked)
            {
                UpdateStationMenu();
            }
            else
            {
                AddTextToList("<color=red>Cannot show this unless docked.</color>");
                UpdateText();
            }
        }
    }

    public void CloseMenu(string menu)
    {
        if(menu != null)
        {
            if (menu == "map")
                mapOverlay.SetActive(false);

            if(menu == "menu")
            {
                Time.timeScale = 1.0f;
                menuOverlay.SetActive(false);
                menuOpen = false;
            }
        }
        else
        {
            Time.timeScale = 1.0f;
            mapOverlay.SetActive(false);
            menuOverlay.SetActive(false);
            menuOpen = false;
        }
    }

    public void AdjustThrottle(string text)
    {
        throttleDisplay.GetComponent<Text>().text = text;
    }

    public void HandleRepair()
    {
        if(PlayerValues.currentHull == PlayerValues.maxHull)
        {
            if (playerInventory.SubtractFromInventory(PlayerValues.repairResource, PlayerValues.RepairHullCost()))
            {
                PlayerValues.currentHull = PlayerValues.maxHull;
            }
            else
            {
                AddTextToList("<color=red>We cannot afford that.</color>");
                UpdateText();
            }
        }
        else
        {
            AddTextToList("<color=red>The ship is at full integrity already.</color>");
            UpdateText();
        }

        UpdateStationMenu();
    }

    public void HandleUpgrade(string upgrade)
    {
        if (upgrade == "power")
        {
            if (playerInventory.SubtractFromInventory(PlayerValues.repairResource, PlayerValues.IncreaseShipPowerCost()))
            {
                PlayerValues.maxPower += PlayerValues.powerIncrease;
                PlayerValues.currentPower = PlayerValues.maxPower;
                PlayerValues.powerLevel++;
            }
            else
                AddTextToList("<color=red>We cannot afford that.</color>");
        }
        else if (upgrade == "hull")
        {
            if (playerInventory.SubtractFromInventory(PlayerValues.repairResource, PlayerValues.IncreaseShipHullCost()))
            {
                PlayerValues.maxHull += PlayerValues.hullIncrease;
                PlayerValues.currentHull = PlayerValues.maxHull;
                PlayerValues.hullLevel++;
            }
            else
                AddTextToList("<color=red>We cannot afford that.</color>");
        }
        else
            AddTextToList("<color=red>Invalid refill type specified.</color>");

        UpdateText();
        UpdateStationMenu();
    }

    public void GameOver(string reason)
    {
        gameOver = true;
        gameOverOverlay.SetActive(true);
        gameOverText.text = "<color=red>" + reason + "</color>" + "\nYou survived for: <color=green>" + Time.timeSinceLevelLoad.ToString("F0") + "</color> seconds.";

        Destroy(playerController.gameObject);
    }

    public void HandleRestart()
    {
        Scene current = SceneManager.GetActiveScene();
        SceneManager.LoadScene(current.buildIndex);
    }

    public void HandleExit()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void Help()
    {
        AddTextToList("<color=red>Available Commands:</color>");
        AddTextToList("<color=orange>dock, move<left/right/up/down></color>");
        AddTextToList("<color=orange>pickup, scan, shoot <target></color>");
        AddTextToList("<color=orange>show <map/menu></color>");
        AddTextToList("<color=orange>throttle <min/max/up/down></color>");
    }
}