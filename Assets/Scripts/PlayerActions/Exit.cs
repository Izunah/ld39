﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="LD39/PlayerActions/Exit")]
public class Exit : PlayerAction 
{
    public override void DoAction(GameController gameController, string[] separatedText)
    {
        if (gameController.gameOver || gameController.menuOpen)
        {
            gameController.HandleExit();
        }
        else
            gameController.AddTextToList("<color=red>You need to exit from a menu.</color>");
    }
}